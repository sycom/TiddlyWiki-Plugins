_[Main repository][repo] is now hosted by [Codeberg][codeberg] Gitea instance. You may find replicant, as on github or gitlab, but they're mirrored... Please, [report bug and comment on codeberg][issues]_

# TiddlyWiki Plugins

is a playing space for [TiddlyWiki][tiddlywiki] plugins. Try the demos on [gitlab.io pages][gl-pages] and [github.io pages][demo]. Drag drop the last release from those to your TiddlyWiki to enhance it. You may also find some unreleased work here around : not all experiments are a success.

## [Atom feed plugin](./plugins/sycom/atom-feed) - 0.0

A very early release of a plugin that creates an atom feed for your wiki. Will possibly help people to follow your news. And maybe will help for SEO?

## [Feather icons](./plugins/sycom/feather-icons) - 0.2

The feather icons plugin aims to integrate feather icons library in TiddlyWiki. Feather is a very open alternative to classic Font Awesome library, using svg sprites instead of classical font implementation. Pretty lightweight and elegant.

## [Leaflet plugin](./plugins/sycom/leaflet) - 0.8

The leaflet plugin is an attempt to integrate the leaflet js library in TiddlyWiki in order to display geographical purpose tiddlers.

For now you can display an interactive map, select size, location and zoom, marker and background. You can also display simple geographical data, stored as json or even stored in metadata fields from tiddler(s) : point(s), polygon(s) and/or polyline(s). Points are clustered when needed (but you can disable clustering) and you have some ways to choose colors.

## [Some macros and widget](./ext/modules)

* **geolocation widget** is part of Leaflet plugin but may also bring geolocation capabilities to your wiki
* **other-tree macro** is a variant of the core _tree macro_

## Outdated
### [G-Analytics plugin](./plugins/sycom/g-analytics) - 1.2

A tool for having stats of wikis visitors. Created before the [official one][tw-ga-official] revived. Mine implements Do Not Track and enables individual tiddlers tracking.

//Does not implement the new GA-4 tracker and is by the way outdated. Also the official tracker is back, so...//

### * **i18n macro**

Enables multinlingual capabilities without hacking existing `<<lingo>>` mechanism

## Sources / licenses

* [TiddlyWiki][tiddlywiki] uses a BSD 3-Clause [License][tw-license]
* All my projects for TiddlyWiki have [a similar one](LICENSE.md). Please refer to each plugin directory for more informations about sources.

## sycom's plugins on the web

* **[codeberg][repo]** ([issues][issues])
  * _pages_ > **[codeberg page][pages]** 

* May be outdated
    * [gitlab][gitlab]
      * _release_ > [gitlab.io page][gl-pages]
    * [github][github]
      * _gh-pages_ > [github.io page][gh-pages]
    * [framagit][framagit]
      * _release_ > [frama.io page][fm-pages]

[repo]: https://codeberg.org/sycom/TiddlyWiki-Plugins
[issues]: https://codeberg.org/sycom/TiddlyWiki-Plugins/issues
[pages]: https://sycom.codeberg.page/TiddlyWiki-Plugins/
[framagit]: https://framagit.org/sycom/TiddlyWiki-Plugins
[fm-pages]: http://sycom.frama.io/TiddlyWiki-Plugins
[gitlab]: https://gitlab.com/sycom/TiddlyWiki-Plugins
[gl-pages]: http://sycom.gitlab.io/TiddlyWiki-Plugins
[github]: https://github.com/sycom/TiddlyWiki-Plugins
[gh-pages]: http://sycom.github.io/TiddlyWiki-Plugins
[tw-ga-official]: https://github.com/sycom/TiddlyWiki5/plugins/tiddlywiki/googleanalytics
[tw-license]: https://github.com/Jermolene/TiddlyWiki5/blob/master/license

[framasoft]: http://framasoft.org
[tiddlywiki]: http://tiddlywiki.com
