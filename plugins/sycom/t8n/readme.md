The **t8n plugin** is a _proof of concept_ of a translating tool for [TiddlyWiki][tiddlywiki]. It provides processes and tools for seamlessly displaying translations of tiddlers in a wiki when switching from a language to another.

* Translation tiddlers are refering to their original with the `t8n-source` field, and declare their language with the `language` field ;
* **t8n macros** aims to replace `<$transclude ... />` and `{{ ... }}` in the translation tiddlers ;

See _usage_ and _settings_ for details.

[source code][source]

[tiddlywiki]: https://tiddlywiki.com
[source]: https://codeberg.org/sycom/TiddlyWiki-Plugins/
