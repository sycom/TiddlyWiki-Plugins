The [i18n plugin](#$:/plugins/sycom/i18n) is a work in progress attempt to enable translation capabilities to your wikis.

## Sources / licenses
* **i18n plugin** is released under similar [BSD 3-Clause license][license] as TiddlyWiki

[license]: https://gitlab.com/sycom/TiddlyWiki-Plugins/LICENSE.md
