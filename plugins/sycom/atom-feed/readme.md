The [atom feed plugin](#$:/plugins/sycom/atom-feed) will help adding an atom ([RFC 4287][RFC]) feed to your TiddlyWiki when you _build_ it. It will create an `atom.xml` file with the list of latest tiddlers of your wiki.

## Sources / licenses
* **atom feed plugin** is released under similar [BSD 3-Clause license][license] as TiddlyWiki
* atom feed plugin icon is based upon [feather icon][feather] library

[RFC]: https://tools.ietf.org/html/rfc4287
[license]: https://framagit.org/sycom/TiddlyWiki-Plugins/LICENSE.md
[feather]: https://feathericons.com/
