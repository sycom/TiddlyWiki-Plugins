The [feather icons plugin](#$:/plugins/sycom/feather-icons) is a work in progress integration of the [feather icons][feather] library into TiddlyWiki.

## Sources / licenses

* [feather icons][feather] (_4.29.0_) license MIT
  * extended feather icons library for the plugin by @sycom is also under MIT license 
* **feather icons plugin** is released under similar [BSD 3-Clause license][license] as TiddlyWiki

[feather]: https://feathericons.com/
[license]: https://codeberg.org/sycom/TiddlyWiki-Plugins/LICENSE.md
