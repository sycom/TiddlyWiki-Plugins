created: 20191007165554914
creator: Sylvain
modified: 20231031172747006
modifier: Sylvain
tags: t8n tool
title: t8n translation helper
type: text/vnd.tiddlywiki

\define t8n-source() {{{ [<currentTiddler>get[t8n-source]!is[missing]][<currentTiddler>get[t8n-source]is[shadow]] }}}
\define o_date() {{{ [<currentTiddler>get[t8n-source]!is[missing]get[modified]][<currentTiddler>get[t8n-source]is[shadow]get[modified]] }}}

<$let wikilang={{{ [{$:/language}get[name]] }}} wikilang-main={{{ [[$:/language]get[name]else[en-GB]] }}}>

!! translations in current language {{$:/core/ui/Buttons/language}} and their originals

All tiddlers with `language` field set to ''<<wikilang>>'' and the original tiddler they are associated to. //out// column check respective dates to identify possibly outdated translations.

<table class="tc-table tc-max-width"><thead><th>translation in <<wikilang>></th><th>//original//</th><th>//out?//</th></thead><tbody>
<$list filter="[language<wikilang>has[t8n-source]]"><$let t_date={{{ [<currentTiddler>get[modified]] }}}>
<tr><td><$link /></td><td><<t8n-source>><$text text={{{ [get[t8n-source]]:then[get[t8n-source]get[language]else<wikilang-main>addprefix[ (]addsuffix[)]] }}}/></td><td><$let o_date={{{ [<currentTiddler>get[t8n-source]!is[missing]get[modified]][<currentTiddler>get[t8n-source]is[shadow]get[modified]] }}}><$text text={{{ [<t_date>compare:date:lt<o_date>then[-!x!-]]}}} /></$let></td></tr></$let>
</$list>
</tbody></table>

!! //original// tiddlers in ''<<wikilang>>'' and their translations

Tiddler are considered original if they can be found in a `t8n-source` field of another tiddler. 

<table class="tc-table tc-max-width"><thead><th>original tiddler</th><th>translations (out?)</th></thead><tbody>
<$list filter="[<wikilang>compare:string:eq<wikilang-main>]:then[has[t8n-source]get[t8n-source]!has[language]][has[t8n-source]get[t8n-source]language<wikilang>]:else[has[t8n-source]get[t8n-source]language<wikilang>]+[unique[]]"><$let o_date={{!!modified}}>
<tr><td><$link/></td><td><ul><$list filter="[t8n-source<currentTiddler>]"><$let t_date={{{ [<currentTiddler>get[modified]] }}}>
<li>''{{!!language}}'' : <$link/><$text text={{{ [<t_date>compare:date:lt<o_date>then[outdated?]addprefix[ (]addsuffix[)]]}}} /></li></$let></$list></ul></td></tr>
</$let></$list>
</tbody></table>

!! //Missing// translations in ''<<wikilang>>''

All //non system// tiddlers that are not in <<wikilang>>, and not having a translation (or original) in <<wikilang>>. And all //system// tiddlers that do have a translation but not in <<wikilang>>.

<$list filter="[<wikilang>compare:string:ne<wikilang-main>]">
    <$list filter="[!has[language]!is[system]][all[]get[t8n-source]]+[unique[]]-[language<wikilang>]">

{{{ [all[]language<wikilang>t8n-source<currentTiddler>then[]else<currentTiddler>] }}}
   </$list>
</$list>
<$list filter="[<wikilang>compare:string:eq<wikilang-main>]">

<$list filter="[!is[system]][get[t8n-source]][!language<wikilang>]+[unique[]]:intersection[has[language]!language<wikilang>]">

{{{ [<currentTiddler>get[t8n-source]is[missing]!is[system]then<currentTiddler>] }}}
</$list> 
</$list>
</$let>