Le **plugin t8n** est un _démonstrateur_ d'un outil de traduction pour[TiddlyWiki][tiddlywiki]. Il fournit des outils et processus pour afficher, de manière transparente pou l'utilisateur, des traductions de tiddlers dans un wiki lorsque l'on passe d'une langue à l'autre.

* les tiddlers traduction font référence à leur original par l'intermédiaire du champ `t8n-source`, et indiquent leur langue de rédaction à l'aide du champ `language` ;
* les **macros t8n** ont vocation à remplacer `<$transclude ... />` et `{{ ... }}` dans les tiddlers traduction ;

Voir _usage_ et _settings_ pour plus de détail.

[source code][source]

[tiddlywiki]: https://tiddlywiki.com
[source]: https://codeberg.org/sycom/TiddlyWiki-Plugins/