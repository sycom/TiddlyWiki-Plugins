Le [plugin feather icons](#$:/plugins/sycom/feather-icons) est un travail en cours pour l'intégration de la librairie d'icônes [feather][feather] dans TiddlyWiki.
## Sources / licences
* [feather icons][feather] (_4.29.0_) licence MIT
    * la librairie d'icones complémentaires pour le plugin par @sycom est sous licence MIT
* le  **plugin feather icons** est sous une [licence BSD 3-Clause][license] similaire à celle de TiddlyWiki


[feather]: https://feathericons.com/
[license]: https://gitlab.com/sycom/TiddlyWiki-Plugins/LICENSE.md
