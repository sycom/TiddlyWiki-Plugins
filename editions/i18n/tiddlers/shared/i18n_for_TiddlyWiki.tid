created: 20191009135253649
modified: 20220821205758466
tags: i18n documentation
title: i18n for TiddlyWiki
type: text/vnd.tiddlywiki

This wiki is experimenting TiddlyWiki translation capabilities. The main goal is to provide an efficient tool for translating plugins and documentations without running [[lingo macro|https://tiddlywiki.com/#lingo%20Macro]].

!! Where we are ?

To keep it short, we still are at the _proof of concept_ step. We now have the [[i18n Plugin]] that modify the way tiddlers are displayed (see //Mechanism// below) and a [[i18n Macro]] to replace classic transclusion inside WikiText.

It's a good tool for translating tiddlers. But, for now, translated tiddlers are not searchable, which is a clear limit for a real multilingual mechanism in TiddlyWiki.

As translations are basically stored in hidden tiddlers, there is also an [[i18n translation helper]] (that still needs a lot ironing and might become part of the $:/ControlPanel ?)

!! Mechanism

When displaying a tiddler, the wiki will check if a translation tiddler can be found :

* With the `original` field set to the current Tiddler title
* With the `lang` field set to the current language of the wiki.

If so the translation wiki will be displayed instead of the current one. And the `caption` field of the translation will be used as title.

The `title` of the translation tiddler can be anything. Though, a good convention would be to name it as an _i18n_ tiddler with some context informations : `$:/i18n/`''lang''`/`''context/path''`/` `caption` 

!! History and thoughts

At first there has been a //javascript macro//, and some classic macro as _proof of concept. Through usage (//eat your own dogfood//) and discussions on the github repository ([[#2722 PR|https://github.com/Jermolene/TiddlyWiki5/pull/2722]], [[#4179 issue|https://github.com/Jermolene/TiddlyWiki5/issues/4179]]) and a dedicated [[CodiMD pad|https://demo.codimd.org/ak7D5_2tQRmPt60zvbJkag]], I imagined the new mechanism in which you don't ''have to'' modify original tiddlers to get them translated.

!! Next steps :

* ~~modify //editTemplates// so the author can translate tiddlers or modify translation~~ ;
* imagine a way to display translated tiddlers in lists (at least in //sideBar//) ;
* imagine a way to search through translation ;
* ...
