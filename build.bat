:: I do use this build when working on Codeberg :
:: - build with this in main
:: - checkout to pages branch
:: - copy ../public to the branch and commit/push
:: - switch back to main
@echo off
goto :cleanup

:cleanup
    del .\public /s /q
    del ..\public /s /q
    goto :build

:build
    call tw atom-feed build
    call tw feather-icons build
    call tw leaflet build
    call tw t8n build
REM    call tw g-analytics build   
REM    call tw i18n build
    call tw collection build
    goto :collect

:collect
    xcopy .\files\* .\public\ /s /i
    xcopy .\public\* ..\public\ /s /i